FROM openjdk:8-alpine
COPY target/*.jar projex.jar
ENTRYPOINT ["java","-jar","projex.jar"]
