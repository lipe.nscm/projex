package br.com.projex.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
@Table(name = "MATR_ALU")
@IdClass(MatriculaAlunoId.class)
@NoArgsConstructor
@AllArgsConstructor
public class MatriculaAluno implements Serializable{

    @Column(name = "COD_PESS_FIC")
    @NonNull
    @Id
    private String codigoPessoaFisica;

    @Column(name = "NUM_MATRI_ALU")
    @NonNull
    @Id
    private String numeroMatriculaAluno;

    @Column(name = "ANO_INGR_ALU")
    @NonNull
    @Id
    private String anoIngressoAluno;



    @Column(name = "NOM_COMP_ALU")
    private String nomeCompletoAluno;
    @Column(name = "DATA_NASC_ALU")
    private Date dataNascimentoAluno;



}


