package br.com.projex.entities;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class MatriculaAlunoId implements Serializable {

    private String codigoPessoaFisica;

    private String numeroMatriculaAluno;

    private String anoIngressoAluno;





}