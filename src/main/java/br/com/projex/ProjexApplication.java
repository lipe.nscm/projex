package br.com.projex;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ProjexApplication {

    public static void main(String[] args) {

        SpringApplication.run(ProjexApplication.class, args);
    }

}
