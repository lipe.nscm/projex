package br.com.projex.controller;

import br.com.projex.dto.MatriculaAlunoDto;
import br.com.projex.entities.MatriculaAluno;
import br.com.projex.repository.MatriculaAlunoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MatriculaAlunoController {

    private final MatriculaAlunoRepository matriculaAlunoRepository;

    @PostMapping("/matricula-aluno")
    public void salvarMatriculaAluno(@RequestBody MatriculaAlunoDto matriculaAlunoDto){
        MatriculaAluno matriculaAluno = new MatriculaAluno();
        matriculaAluno.setNumeroMatriculaAluno(matriculaAlunoDto.getNumeroMatriculaAluno());
        matriculaAluno.setAnoIngressoAluno(matriculaAlunoDto.getAnoIngressoAluno());
        matriculaAluno.setCodigoPessoaFisica(matriculaAlunoDto.getCodigoPessoaFisica());
        matriculaAluno.setNomeCompletoAluno(matriculaAlunoDto.getNomeCompletoAluno());
        matriculaAluno.setDataNascimentoAluno(matriculaAlunoDto.getDataNascimentoAluno());

        matriculaAlunoRepository.save(matriculaAluno);
    }

    @GetMapping("/matricula-aluno")
    public List<MatriculaAluno> listarMatriculasAluno(){
        return (List<MatriculaAluno>) matriculaAlunoRepository.findAll();
    }
}
