package br.com.projex.dto;

import lombok.*;


import java.util.Date;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MatriculaAlunoDto {

    @NonNull
    private String codigoPessoaFisica;
    @NonNull
    private String numeroMatriculaAluno;
    @NonNull
    private String anoIngressoAluno;
    private String nomeCompletoAluno;
    private Date dataNascimentoAluno;
}
