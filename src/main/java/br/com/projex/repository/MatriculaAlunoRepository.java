package br.com.projex.repository;

import br.com.projex.entities.MatriculaAluno;
import br.com.projex.entities.MatriculaAlunoId;
import org.springframework.data.repository.CrudRepository;

public interface MatriculaAlunoRepository extends CrudRepository<MatriculaAluno, MatriculaAlunoId> {
}
